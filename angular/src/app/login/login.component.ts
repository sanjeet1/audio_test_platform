import {
  Component,
  OnInit
} from '@angular/core';
import {
  AuthService
} from './auth.service';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailhasValue = true;
  emailValidate = true;
  passwordhasValue = true;
  validPassword = true;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {}

  loginSubmit(e: Event) {
    e.preventDefault();
    const user = {
      email: e.target[0].value,
      password: e.target[1].value
    };

    // Email and password Validations
    if (!user.email) {
      this.emailhasValue = false;
    } else if (user.password && !user.email) {
      this.emailhasValue = false;
    }

    const pattern = '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
    if (user.email && !user.email.match(pattern)) {
      this.emailValidate = false;
    }

    if (!user.password) {
      this.passwordhasValue = false;
    }

    // Authenticating User to Login
    if (user.email && user.email.match(pattern) && user.password) {
      this.authService.authenticateUser(user).subscribe((data: Response) => {
        console.log(user);
        // console.log(this.validateUser(data[0]));
        if (this.validateUser(data[0])) {
          this.router.navigate(['/dashboard']);
        } else {
          this.validPassword = false;
        }
      }), (err) => {
        console.log(err);
        this.validPassword = false;
        // this.router.navigate(['/login']);
      }
    }
  }


  validateUser(user) {
    if (user.email === 'cfe.sanjeet@mettl.com' && user.password === 'mettl123') {
      return true;
    } else if(!(user.email === 'cfe.sanjeet@mettl.com' && user.password === 'mettl123')){
      return false;
    }
  }

}
