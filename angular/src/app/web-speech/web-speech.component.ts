import {
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import {
  SpeechRecognizerService
} from './shared/services/speech-recognizer.service';

import {
  SpeechNotification
} from './shared/model/speech-notification';
import {
  SpeechError
} from './shared/model/speech-error';
import {
  ActionContext
} from './shared/model/strategy/action-context';
import {
  TestdataService
} from '../testdata.service';
@Component({
  selector: 'wsa-web-speech',
  templateUrl: './web-speech.component.html',
  styleUrls: ['./web-speech.component.css']
})
export class WebSpeechComponent implements OnInit {
  finalTranscript: string = '';
  recognizing: boolean = false;
  notification: string;
  languages: string[] = ['en-US', 'es-ES'];
  currentLanguage: string;
  actionContext: ActionContext = new ActionContext();
  testData;

  constructor(private changeDetector: ChangeDetectorRef,
    private speechRecognizer: SpeechRecognizerService,
    private dataService: TestdataService) {}

  ngOnInit() {
    this.currentLanguage = this.languages[0];
    this.speechRecognizer.initialize(this.currentLanguage);
    this.initRecognition();
    this.notification = null;

    // get data
    this.testData = this.dataService.selectedTest;
    console.log(this.testData);
    this.startExecuting();
  }

  startExecuting() {
    let event = {
      timeStamp: +new Date
    }
    setTimeout(() => {
      if (this.recognizing) {
        this.speechRecognizer.stop();
        return;
      }
      console.log(event);
      this.speechRecognizer.start(event.timeStamp);
    }, 5000);

  }




  startButton(event) {
    if (this.recognizing) {
      this.speechRecognizer.stop();
      return;
    }
    // console.log(event);
    // this.speechRecognizer.start(event.timeStamp);
  }

  onSelectLanguage(language: string) {
    this.currentLanguage = language;
    this.speechRecognizer.setLanguage(this.currentLanguage);
  }

  ValidateContent(content) {
    var options = ['option A', 'option B', 'option C', 'option d', 'optiondee'];
    options.forEach((el) => {
      if (el == content) {
        if (content === 'option A') {
          document.querySelectorAll('input[type="radio"]')[0]['checked'] = true;
        } else if (content === 'option B') {
          document.querySelectorAll('input[type="radio"]')[1]['checked'] = true;
        } else if (content === 'option C') {
          document.querySelectorAll('input[type="radio"]')[2]['checked'] = true;
        } else if (content === 'option d' || 'optiondee') {
          document.querySelectorAll('input[type="radio"]')[3]['checked'] = true;
        }
      }
      return true;
    });
  }

  checkOption(content) {
    this.ValidateContent(content);
    this.speechRecognizer.stop();

  }

  private initRecognition() {
    this.speechRecognizer.onStart()
      .subscribe(data => {
        this.recognizing = true;
        this.notification = 'I\'m listening...';
        this.detectChanges();
      });

    this.speechRecognizer.onEnd()
      .subscribe(data => {
        this.recognizing = false;
        this.detectChanges();
        this.notification = null;
      });



    this.speechRecognizer.onResult()
      .subscribe((data: SpeechNotification) => {
        const message = data.content.trim();
        if (data.info === 'final_transcript' && message.length > 0) {
          this.checkOption(data.content);
          this.finalTranscript = `${this.finalTranscript}\n${message}`;
          this.actionContext.processMessage(message, this.currentLanguage);
          this.detectChanges();
          this.actionContext.runAction(message, this.currentLanguage);
        }
      });

    this.speechRecognizer.onError()
      .subscribe(data => {
        switch (data.error) {
          case SpeechError.BLOCKED:
          case SpeechError.NOT_ALLOWED:
            this.notification = `Cannot run the demo.
            Your browser is not authorized to access your microphone. Verify that your browser has access to your microphone and try again.
            `;
            break;
          case SpeechError.NO_SPEECH:
            this.notification = `No speech has been detected. Please try again.`;
            break;
          case SpeechError.NO_MICROPHONE:
            this.notification = `Microphone is not available. Plese verify the connection of your microphone and try again.`
            break;
          default:
            this.notification = null;
            break;
        }
        this.recognizing = false;
        this.detectChanges();
      });
  }

  detectChanges() {
    this.changeDetector.detectChanges();
  }




}
