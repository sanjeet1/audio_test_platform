import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule, Routes } from '@angular/router';

import { AuthService } from './login/auth.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';

import { MaterialModule } from './shared/material/material.module';
import { WebSpeechComponent } from './web-speech/web-speech.component';
import { SpeechSynthesizerService } from './web-speech/shared/services/speech-synthesizer.service';
import { SpeechRecognizerService } from './web-speech/shared/services/speech-recognizer.service';
import { SharedModule } from './shared/shared.module';

import { TestdataService } from './testdata.service';



const appRoutes: Routes = [
  { path: '', component: LoginComponent }, 
   { path: 'login', component: LoginComponent },
   { path: 'dashboard', component: DashboardComponent },
   { path: 'portal', component: WebSpeechComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    WebSpeechComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [AuthService, SpeechSynthesizerService, SpeechRecognizerService, TestdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
