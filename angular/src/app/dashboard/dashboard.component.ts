import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';

import { map, filter, scan } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TestdataService } from '../testdata.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  configUrl = environment.apiUrl
  testList;

  constructor(private http: HttpClient,
  private dataService:TestdataService,
private router: Router ) { }

  ngOnInit() {
  this.getTestList().subscribe((res) => {
     this.testList = res;
     console.log(this.testList);
    },
    (err)=> {
      console.log(err);
    })
  }


  getTestList() {
    return this.http.get(this.configUrl+'/testlist');
  }

  testSelect(test) {
    this.dataService.selectedTest = test;
    this.router.navigate(['/portal'])
    console.log(this.dataService.selectedTest);
  }

}
