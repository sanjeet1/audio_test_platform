var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var config = require('../config/config');
var db = mongojs(config.MONGO_URL, ['testlist']);


/* GET users listing. */
router.get('/', function (req, res, next) {
  db.testlist.find((err, users) => {
    if (err) {
      res.send(err);
      console.log('err');
    } else {
      res.json(users);
    }
  });
});

module.exports = router;