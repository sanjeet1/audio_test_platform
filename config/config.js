const dotenv = require('dotenv');

const env = process.env.NODE_ENV || 'dev';
console.log('Starting server with environment:', env);


result = dotenv.config();
if (result.error) {
  console.log('No .env file found!');
  // throw result.error;
}

const finalConfig = process.env;
// console.log('Final config', finalConfig);
// console.log('Process env after loading local config:', process.env);
module.exports = finalConfig;